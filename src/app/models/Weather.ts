export interface WeatherDetails{
    name?: String;
    temperature?: String;
    wind?: String;
    humidity?: String;
}