import { Component, OnInit } from '@angular/core';
import { WeatherDetails } from '../models/Weather';

export const weatherDetailsList: WeatherDetails[] = [
  {
    name: 'Chennai',
    temperature: '37',
    wind: '22.2Kmph',
    humidity: '9%',
  },
  {
    name: 'Delhi',
    temperature: '23',
    wind: '29.2Kmph',
    humidity: '5%',
  },
  {
    name: 'Pune',
    temperature: '37',
    wind: '20.2Kmph',
    humidity: '4%',
  },
  {
    name: 'Bangalore',
    temperature: '36',
    wind: '29.2Kmph',
    humidity: '6%',
  },
  {
    name: 'Karnataka',
    temperature: '44',
    wind: '21.2Kmph',
    humidity: '3%',
  },
  {
    name: 'Kerala',
    temperature: '26',
    wind: '23.2Kmph',
    humidity: '5%',
  },
  {
    name: 'Mumbai',
    temperature: '27',
    wind: '24.2Kmph',
    humidity: '7%',
  },
  {
    name: 'Bhopal',
    temperature: '29',
    wind: '25.2Kmph',
    humidity: '4%',
  },
  {
    name: 'Indore',
    temperature: '33',
    wind: '28.2Kmph',
    humidity: '5%',
  },
  {
    name: 'Lucknow',
    temperature: '32',
    wind: '25.2Kmph',
    humidity: '65%',
  },
  {
    name: 'Salem',
    temperature: '31',
    wind: '29.2Kmph',
    humidity: '8%',
  },
];

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.css'],
})
export class WeatherDetailsComponent implements OnInit {
  cityName: String;
  weatherReport: WeatherDetails | undefined;
  isInitial: boolean;

  constructor() {}

  ngOnInit(): void {}

  onModelChange(event: String) {
    for (let index = 0; index < weatherDetailsList?.length; index++) {
      if (
        weatherDetailsList[index]?.name?.toUpperCase() === event?.toUpperCase()
      ) {
        this.weatherReport = weatherDetailsList[index];
        break;
      } else {
        this.weatherReport = undefined;
        this.isInitial = true;
      }
    }
    if (event === '') {
      this.isInitial = false;
    }
  }
}
